import numpy as np
import cv2
import time
from threading import Thread, Lock
import threading
import socket
import socketio
import json

class VideoStream:
    def __init__(self, src):
        global Connect
        print("Starting Stream")
        self.stream = cv2.VideoCapture(src)
        self.grabbed, self.frame = self.stream.read()
        self.started = False
        self.read_lock = Lock()

    def start(self):
        if self.started:
            print("already started!!")
            return None
        self.started = True
        self.thread = Thread(target=self.update, args=())
        self.thread.start()
        return self

    def update(self):
        while self.started:
            (grabbed, frame) = self.stream.read()
            self.read_lock.acquire()
            self.grabbed, self.frame = grabbed, frame
            self.read_lock.release()

    def read(self):
        frame_trans = None
        if (self.frame is not None):
            self.read_lock.acquire()
            frame_trans = self.frame.copy()
            # Resize the image for processing
            frame_trans = cv2.resize(frame_trans, (360, 640))
            self.read_lock.release()

        return frame_trans

    def stop(self):
        self.started = False
        self.thread.join()

    def __exit__(self, exc_type, exc_value, traceback):
        self.stream.release()

class Process:
    frame1 = None

    def Run(self, name, src):
        vs = VideoStream(src).start()
        while True :
            # Check the stream connection
            Process.frame1 = vs.read()
            time.sleep(0.05)
            frame2 = vs.read()
            
            if (Process.frame1 is None or frame2 is None or vs.grabbed is False):
                print(name + " failed!")
                cv2.destroyAllWindows()
                vs.stop()
                time.sleep(5)
                vs = VideoStream(src).start()
                continue

            # Analysis image here!
            diff = cv2.absdiff(Process.frame1, frame2)
            gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(gray, (5,5), 0)
            _, thresh = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY)
            dilated = cv2.dilate(thresh, None, iterations=3)
            contours, _ = cv2.findContours(dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            for contour in contours:
                (x, y, w, h) = cv2.boundingRect(contour)
                if cv2.contourArea(contour) < 200:
                    continue
                thread1 = threading.Thread(target=Process().Tracking, args=(x,y,w,h,))
                thread1.start()

            cv2.imshow(name, Process.frame1)

            if cv2.waitKey(1) == 30 :
                cv2.destroyAllWindows()
                continue

    def Tracking(self,x,y,w,h):
        cv2.rectangle(Process.frame1, (x, y), (x+w, y+h), (0, 255, 0), 2)

if __name__ == "__main__" :
    #add name and link of video stream here
    t1 = threading.Thread(target=Process().Run, args=('CCTV2', 'rtsp://192.168.100.15:5540/ch0',))
    t1.start()